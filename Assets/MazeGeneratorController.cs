using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MazeGeneratorController : MonoBehaviour
{
    [SerializeField] private GameObject FirstCorridor;
    [SerializeField] private GameObject SecondCoreidor;

    [SerializeField] private GameObject close_Corridor, winRoom;

    [SerializeField] private GameObject[] Corridors, exits;

    [SerializeField] private Transform Exit, RightCheck, LeftCheck, ForwardCheck;

    [SerializeField] private GameManager GameManagerScript;


    [SerializeField] private bool HasWin = false;

    [SerializeField] private int LevelCounter = 3, pathCounter = 0;

    [SerializeField] private LayerMask Layer;

    public GameObject TempGameObject;



    // Start is called before the first frame update
    void Start()
    {
        exits = GameObject.FindGameObjectsWithTag("Exit");

        GameManagerScript = GameObject.Find("GameManager").GetComponent<GameManager>();

        Corridors = Resources.LoadAll<GameObject>("Prefabs/Rooms");

        int Rnd = UnityEngine.Random.Range(0, 4);

        for (int i = 0; i < 4; i++)
        {
            if (i == Rnd)
            {
                Build(exits[i].transform, 0, LevelCounter, true);
            }
            else
            {
                Build(exits[i].transform, 0, LevelCounter, false);

            }
        }
        for (int i = 0; i < 4; i++)
        {
            if (Physics.CheckSphere(exits[i].transform.position, 3, Layer))
            {
                BuildEnd(exits[i].transform, exits[i].transform.rotation, close_Corridor);
            }
        }


    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }


    public void Build(Transform exit, int q, int d, bool finish)
    {
        if (q > 0 || d > 0)
        {
            bool LeftFreeBool, RightFreeBool, ForwardFreeBool;
            LeftFreeBool = LeftSmallFree(exit);
            RightFreeBool = RightSmallFree(exit);
            ForwardFreeBool = ForwardFree(exit);
            if (LeftFreeBool && RightFreeBool && ForwardFreeBool)
            {
                GameObject Temp = BuildRandom(exit, exit.rotation);

                if (Temp.transform.CompareTag("T_Junction"))
                {
                    pathCounter = pathCounter++;
                    Build(Temp.transform.Find("ExitPoint_Right").transform, q, d - 1, finish);
                    Build(Temp.transform.Find("ExitPoint_Left").transform, q, d - 1, finish);
                }
                else if (Temp.transform.name.Equals("Question_Room"))
                {
                    Build(Temp.transform.Find("ExitPoint").transform, q - 1, d - 1, finish);
                }
                else
                {
                    Build(Temp.transform.Find("ExitPoint").transform, q, d - 1, finish);
                }
            }
            else if (!ForwardFreeBool && LeftFreeBool && RightFreeBool)
            {
                GameObject Temp = BuildLeftRightJucntion(exit, exit.rotation);

                if (Temp.transform.CompareTag("T_Junction"))
                {
                    pathCounter = pathCounter++;
                    Build(Temp.transform.Find("ExitPoint_Right").transform, q, d - 1, finish);
                    Build(Temp.transform.Find("ExitPoint_Left").transform, q, d - 1, finish);
                }
                else
                {
                    Build(Temp.transform.Find("ExitPoint").transform, q, d - 1, finish);
                }
            }
            else if (ForwardFreeBool && LeftFreeBool && !RightFreeBool)
            {
                GameObject Temp = BuildRandomLeftForward(exit, exit.rotation);

                Build(Temp.transform.Find("ExitPoint").transform, q, d - 1, finish);

            }
            else if (ForwardFreeBool && !LeftFreeBool && RightFreeBool)
            {
                GameObject Temp = BuildRandomRightForward(exit, exit.rotation);
                Build(Temp.transform.Find("ExitPoint").transform, q, d - 1, finish);
            }
            else if (!ForwardFreeBool && LeftFreeBool && !RightFreeBool)
            {
                GameObject Temp = BuildLeftSmall(exit, exit.rotation);
                Build(Temp.transform.Find("ExitPoint").transform, q, d - 1, finish);
            }
            else if (!ForwardFreeBool && !LeftFreeBool && RightFreeBool)
            {
                GameObject Temp = BuildRightSmall(exit, exit.rotation);
                Build(Temp.transform.Find("ExitPoint").transform, q, d - 1, finish);
            }
            else if (ForwardFreeBool && !LeftFreeBool && !RightFreeBool)
            {
                GameObject Temp = BuildForward(exit, exit.rotation);
                Build(Temp.transform.Find("ExitPoint").transform, q, d - 1, finish);

            }
            else if (!LeftFreeBool && !RightFreeBool && !ForwardFreeBool)
            {
                if (pathCounter > 0)
                {
                    BuildEnd(exit, exit.rotation, close_Corridor);
                    pathCounter--;
                }
                else
                {
                    BuildEnd(exit, exit.rotation, close_Corridor);

                }

                if (pathCounter == 0 && !HasWin)
                {
                    BuildEnd(exit, exit.rotation, winRoom);
                    HasWin = true;
                }
                else
                {
                    BuildEnd(exit, exit.rotation, close_Corridor);

                }

            }
        }
        else
        {
            if (finish && !HasWin)
            {
                HasWin = true;
                BuildEnd(exit, exit.rotation, winRoom);
            }
            else
            {
                BuildEnd(exit, exit.rotation, close_Corridor);

            }
        }

    }
    public void BuildEnd(Transform exit, Quaternion lastRot, GameObject tempRoom)
    {

        Quaternion temp = Quaternion.Euler(0, -180, 0);
        Instantiate(tempRoom, exit.position, lastRot * temp);
    }
    public GameObject BuildRandom(Transform exit, Quaternion lastRot)
    {
        int temp;
        temp = Random.Range(0, Corridors.Length);

        while (Corridors[temp].CompareTag("Closed_Corridor"))
        {
            temp = Random.Range(0, Corridors.Length);
        }
        TempGameObject = Instantiate(Corridors[temp], exit.position, lastRot);

        return TempGameObject;


    }
    public GameObject BuildLeftRightJucntion(Transform exit, Quaternion lastRot)
    {
        int temp;
        temp = Random.Range(0, Corridors.Length);

        while (Corridors[temp].CompareTag("Closed_Corridor") && !Corridors[temp].transform.CompareTag("Open_Corridor"))
        {
            temp = Random.Range(0, Corridors.Length);
        }

        TempGameObject = Instantiate(Corridors[temp], exit.position, lastRot);
        return TempGameObject;

    }
    public GameObject BuildRandomLeftForward(Transform exit, Quaternion lastRot)
    {
        int temp;
        temp = Random.Range(0, Corridors.Length);

        while (!Corridors[temp].CompareTag("Open_Corridor") && !Corridors[temp].CompareTag("Right_Corner"))
        {
            temp = Random.Range(0, Corridors.Length);
        }

        TempGameObject = Instantiate(Corridors[temp], exit.position, lastRot);

        return TempGameObject;

    }
    public GameObject BuildRandomRightForward(Transform exit, Quaternion lastRot)
    {
        int temp;
        temp = Random.Range(0, Corridors.Length);

        while (!Corridors[temp].CompareTag("Open_Corridor") && !Corridors[temp].CompareTag("Left_Corner"))
        {
            temp = Random.Range(0, Corridors.Length);
        }

        TempGameObject = Instantiate(Corridors[temp], exit.position, lastRot);


        return TempGameObject;

    }
    public GameObject BuildForward(Transform exit, Quaternion lastRot)
    {
        int temp;
        temp = Random.Range(0, Corridors.Length);

        while (!Corridors[temp].CompareTag("Open_Corridor"))
        {
            temp = Random.Range(0, Corridors.Length);
        }

        TempGameObject = Instantiate(Corridors[temp], exit.position, lastRot);

        return TempGameObject;

    }
    public GameObject BuildRightSmall(Transform exit, Quaternion lastRot)
    {
        int temp;
        temp = Random.Range(0, Corridors.Length);

        while (!Corridors[temp].CompareTag("Right_Corner"))
        {
            temp = Random.Range(0, Corridors.Length);
        }

        TempGameObject = Instantiate(Corridors[temp], exit.position, lastRot);

        return TempGameObject;

    }
    public GameObject BuildLeftSmall(Transform exit, Quaternion lastRot)
    {
        int temp;
        temp = Random.Range(0, Corridors.Length);

        while (!Corridors[temp].CompareTag("Left_Corner"))
        {
            temp = Random.Range(0, Corridors.Length);
        }

        TempGameObject = Instantiate(Corridors[temp], exit.position, lastRot);

        exit = TempGameObject.transform.Find("ExitPoint").transform;

        lastRot = lastRot * exit.localRotation;

        return TempGameObject;

    }
    public bool RightSmallFree(Transform exit)
    {
        ForwardCheck = exit.transform.Find("ForwardCheck").transform;
        RightCheck = exit.transform.Find("RightCheck").transform;
        bool ValueToReturn = true;

        if (Physics.CheckSphere(RightCheck.position, 0.5f) || Physics.Linecast(transform.position, ForwardCheck.position))
        {
            ValueToReturn = false;
        }

        return ValueToReturn;
    }
    public bool LeftSmallFree(Transform exit)
    {
        ForwardCheck = exit.transform.Find("ForwardCheck").transform;
        LeftCheck = exit.transform.Find("LeftCheck").transform;
        bool ValueToReturn = true;

        if (Physics.CheckSphere(LeftCheck.position, 5f) || Physics.Linecast(transform.position, ForwardCheck.position))
        {
            ValueToReturn = false;
        }

        return ValueToReturn;

    }
    public bool ForwardFree(Transform exit)
    {
        ForwardCheck = exit.transform.Find("ForwardCheck").transform;
        bool ValueToReturn = true;

        if (Physics.CheckSphere(ForwardCheck.position, 5f) || Physics.Raycast(exit.position, -exit.forward, 10, Layer) || Physics.Linecast(exit.position, ForwardCheck.position))
        {
            ValueToReturn = false;
        }


        return ValueToReturn;


    }
}
